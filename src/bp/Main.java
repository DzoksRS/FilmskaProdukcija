package bp;

import bp.gui.BaseController;
import bp.gui.WelcomeScreenController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("gui/WelcomeScreen.fxml"));
		TabPane root = (TabPane) loader.load();
		BaseController control = loader.<WelcomeScreenController>getController();
		control.setPrimaryStage(primaryStage);
		Scene scene = new Scene(root);
		primaryStage.setTitle("Filmska Produkcija");
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
