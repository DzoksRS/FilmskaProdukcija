package bp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.dto.ZanrDTO;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ZanrDAO {
	
	public static void dodajZanr(Integer idDjela,Integer idZanra) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajZanr(?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idDjela);
			cs.setInt(2, idZanra);
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
	public static ObservableList<ZanrDTO> zanroviDjela(int idDjela){
		ObservableList<ZanrDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT IdZanra,NazivZanra FROM zanr_djela natural join zanr where IdDjela=?";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idDjela);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new ZanrDTO(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
	public static ObservableList<ZanrDTO> zanrovi(){
		ObservableList<ZanrDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM zanr";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new ZanrDTO(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
}
