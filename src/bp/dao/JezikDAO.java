package bp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.dto.JezikDTO;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class JezikDAO {
	public static void dodajJezik(Integer idDjela,Integer idJezika) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajJezik(?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idDjela);
			cs.setInt(2, idJezika);
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
	public static ObservableList<JezikDTO> jeziciDjela(int idDjela){
		ObservableList<JezikDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT IdJezika,NazivJezika FROM jezik_djela natural join jezik where IdDjela=?";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idDjela);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new JezikDTO(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
	public static ObservableList<JezikDTO> jezici(){
		ObservableList<JezikDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM jezik";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new JezikDTO(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
}
