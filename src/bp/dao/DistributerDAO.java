package bp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import bp.dto.DistributerDTO;
import bp.dto.EMailDTO;
import bp.info.DrzavaInfo;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DistributerDAO {
	public static ObservableList<DistributerDTO> distributeri(int idDjela) {
		ObservableList<DistributerDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM distributeri where IdDjela=?";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idDjela);
			rs = cs.executeQuery();

			while (rs.next()) {
				lista.add(new DistributerDTO(rs.getInt(1), rs.getInt(2), rs.getDate(3).toLocalDate(),
						rs.getDate(4).toLocalDate(), rs.getDate(5).toLocalDate(), rs.getInt(6), rs.getString(7),
						rs.getString(8)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}

	public static ObservableList<DrzavaInfo> drzaveDistribucije(int idUgovora) {
		ObservableList<DrzavaInfo> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "select IdDrzave,NazivDrzave from drzava natural join drzava_distribucije where IdUgovora=?;";
		try {
			conn=ConnectionPool.getInstance().checkOut();
			cs=conn.prepareCall(query);
			cs.setInt(1, idUgovora);
			rs=cs.executeQuery();
			while (rs.next()) {
				lista.add(new DrzavaInfo(rs.getInt(1), rs.getString(2)));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}

	public static void dodajDistributera(int kompanija, int djelo, LocalDate datumPotpisa, LocalDate pocetak,
			LocalDate kraj) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajDistributera(?,?,?,?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, kompanija);
			cs.setInt(2, djelo);
			cs.setDate(3, Date.valueOf(datumPotpisa));
			cs.setDate(4, Date.valueOf(pocetak));
			cs.setDate(5, Date.valueOf(kraj));
			cs.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
	}

	public static void dodajDrzavuDistribucije(Integer idDrzave, int idUgovora) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajDrzavuDistribucije(?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idDrzave);
			cs.setInt(2, idUgovora);
			cs.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
	}

}
