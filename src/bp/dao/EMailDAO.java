package bp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.dto.JezikDTO;
import bp.dto.EMailDTO;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class EMailDAO {


	public static ObservableList<EMailDTO> emailiKompanije(Integer idKompanije){
		ObservableList<EMailDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT EMail FROM e_mail_kompanije where IdKompanije=?";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idKompanije);
			rs = cs.executeQuery();
			
			while (rs.next()) {
				lista.add(new EMailDTO(rs.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
	
	public static void dodajEMailKompanije(Integer idKompanije,String email) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajEmailKompanije(?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idKompanije);
			cs.setString(2, email);
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
	
	public static  void azurirajEMail(String email){
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call azurirajEMail(?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setString(1, email);
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
	public static void obrisiEMail(String email){
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call obrisiEMail(?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setString(1, email);
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
}
