package bp.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;

public class SerijaDAO {
	public static Integer dodajSeriju(String ime,Integer godina,Integer jezik,BigDecimal budzet) {
			Integer retValue=0;
			Connection conn = null;
			ResultSet rs = null;
			CallableStatement cs = null;
			String query = "{call dodajSeriju(?,?,?,?,?)}";
			try {
				conn = ConnectionPool.getInstance().checkOut();
				cs = conn.prepareCall(query);
				cs.registerOutParameter(5, Types.INTEGER);
				cs.setString(1, ime);
				cs.setInt(2, godina);
				cs.setInt(3, jezik);
				cs.setBigDecimal(4, budzet);
				cs.executeUpdate();
				retValue=cs.getInt(5);

			} catch (SQLException e) {

				e.printStackTrace();
				DBUtilities.getInstance().showSQLException(e);
			} finally {
				ConnectionPool.getInstance().checkIn(conn);
				DBUtilities.getInstance().close(cs, rs);

			}
			return retValue;
	}
}
