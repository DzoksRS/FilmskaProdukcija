package bp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.dto.OsobaDTO;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class OsobaDAO {

	public static ObservableList<OsobaDTO> osobe() {
		ObservableList<OsobaDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM osoba";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new OsobaDTO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4)==null?null:rs.getDate(4).toLocalDate()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
	public static void dodajOsobu(OsobaDTO osoba) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajOsobu(?,?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setString(1, osoba.getIme());
			cs.setString(2, osoba.getPrezime());
			cs.setDate(3, osoba.getDatumRodjenja()==null?null:Date.valueOf(osoba.getDatumRodjenja()));
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
	public static void azurirajOsobu(OsobaDTO osoba) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call azurirajOsobu(?,?,?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, osoba.getIdOsobe());
			cs.setString(2, osoba.getIme());
			cs.setString(3, osoba.getPrezime());
			cs.setDate(4, osoba.getDatumRodjenja()==null?null:Date.valueOf(osoba.getDatumRodjenja()));
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
}
