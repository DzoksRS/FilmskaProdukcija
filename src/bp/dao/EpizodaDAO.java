package bp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import bp.dto.DistributerDTO;
import bp.dto.EpizodaDTO;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class EpizodaDAO {
	public static ObservableList<EpizodaDTO> epizode(int idDjela,int idSezone) {
		ObservableList<EpizodaDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM epizoda where IdDjela=? and BrojSezone=?";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idDjela);
			cs.setInt(2, idSezone);
			rs = cs.executeQuery();

			while (rs.next()) {
				lista.add(new EpizodaDTO(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getInt(5)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
	
	public static void dodajEpizodu(int serija,int sezona,int epizodaN,String naziv,int trajanje) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajEpizodu(?,?,?,?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, serija);
			cs.setInt(2, sezona);
			cs.setInt(3, epizodaN);
			cs.setString(4, naziv);
			cs.setInt(5, trajanje);
			cs.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
	}
}
