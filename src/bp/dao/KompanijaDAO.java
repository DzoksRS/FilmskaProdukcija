package bp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.dto.KompanijaDTO;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class KompanijaDAO {
	
	public static ObservableList<KompanijaDTO> kompanije(){
		
		ObservableList<KompanijaDTO> lista=FXCollections.observableArrayList();//new ArrayList<>();
		Connection conn = null;
		ResultSet rs=null;
		CallableStatement cs = null;
		String query = "SELECT * FROM kompanija";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			
			while (rs.next()) {
				lista.add(new KompanijaDTO(rs.getInt("IdKompanije"),rs.getString("NazivKompanije"),rs.getString("Adresa")));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
			
		}
		return lista;
	}

	public static void azurirajKompaniju(KompanijaDTO kompanija) {
		Connection conn = null;
		ResultSet rs=null;
		CallableStatement cs = null;
		String query = "{call azurirajKompaniju(?,?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, kompanija.getIdKompanije());
			cs.setString(2,kompanija.getNazivKompanije());
			cs.setString(3, kompanija.getAdresa());
			cs.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
			
		}
	}
	public static void dodajKompaniju(KompanijaDTO kompanija) {
		Connection conn = null;
		ResultSet rs=null;
		CallableStatement cs = null;
		String query = "{call dodajKompaniju(?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setString(1,kompanija.getNazivKompanije());
			cs.setString(2, kompanija.getAdresa());
			cs.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
			
		}
		
	}
}
