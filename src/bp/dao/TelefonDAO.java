package bp.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.dto.JezikDTO;
import bp.dto.TelefonDTO;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class TelefonDAO {


	public static ObservableList<TelefonDTO> telefoniKompanije(Integer idKompanije){
		ObservableList<TelefonDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT Telefon FROM telefon_kompanije where IdKompanije=?";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idKompanije);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new TelefonDTO(rs.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
	
	public static void dodajTelefonKompanije(Integer idKompanije,String telefon) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajTelefonKompanije(?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idKompanije);
			cs.setString(2, telefon);
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
	public static  void azurirajTelefon(String telefon) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call azurirajTelefon(?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setString(1, telefon);
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
	public static void obrisiTelefon(String telefon) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call obrisiTelefon(?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setString(1, telefon);
			cs.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
	}
	
	
}

