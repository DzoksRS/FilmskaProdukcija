package bp.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;

public class FilmDAO {

	public static Integer dodajFilm(String naziv, Integer godina, Integer idDrzave, BigDecimal budzet, Integer trajanje,
			boolean postoji, boolean dioSerijala,String nazivSerijala) {
		Integer retValue=0;
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajNoviFilm(?,?,?,?,?,?,?,?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.registerOutParameter(9, Types.INTEGER);
			cs.setString(1, naziv);
			cs.setInt(2, godina);
			cs.setInt(3, idDrzave);
			cs.setBigDecimal(4, budzet);
			cs.setInt(5, trajanje);
			cs.setBoolean(6, postoji);
			cs.setBoolean(7, dioSerijala);
			cs.setString(8, nazivSerijala);
			cs.executeUpdate();
			retValue=cs.getInt(9);

		} catch (SQLException e) {

			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);

		}
		return retValue;
	}

}
