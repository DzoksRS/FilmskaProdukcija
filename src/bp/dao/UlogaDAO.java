package bp.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import bp.dto.EpizodaDTO;
import bp.dto.UlogaDTO;
import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class UlogaDAO {
	public static ObservableList<UlogaDTO> epizode(int idDjela) {
		ObservableList<UlogaDTO> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM glumci_info where IdDjela=?";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, idDjela);
			rs = cs.executeQuery();

			while (rs.next()) {
				lista.add(new UlogaDTO(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getDate(6)==null?null:rs.getDate(6).toLocalDate(), rs.getDate(7).toLocalDate(), rs.getDate(8).toLocalDate(),
						rs.getDate(9).toLocalDate(), rs.getBigDecimal(10).toString()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}

	public static void dodajUlogu(int osoba, String glumac, int djelo, BigDecimal honorar, LocalDate datumPotpisa,
			LocalDate pocetak, LocalDate kraj) {
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "{call dodajGlumca(?,?,?,?,?,?,?)}";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			cs.setInt(1, osoba);
			cs.setString(2, glumac);
			cs.setInt(3, djelo);
			cs.setBigDecimal(4, honorar);
			cs.setDate(5, Date.valueOf(datumPotpisa));
			cs.setDate(6, Date.valueOf(pocetak));
			cs.setDate(7, Date.valueOf(kraj));
			cs.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
	}
}
