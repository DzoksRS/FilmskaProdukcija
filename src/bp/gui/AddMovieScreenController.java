package bp.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;

import bp.dao.FilmDAO;
import bp.dao.JezikDAO;
import bp.dao.SerijaDAO;
import bp.dao.ZanrDAO;
import bp.dto.JezikDTO;
import bp.dto.ZanrDTO;
import bp.info.DrzavaInfo;
import bp.info.Serijal;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;

import javafx.scene.control.ListView;

import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Labeled;

public class AddMovieScreenController extends BaseController implements Initializable {
	@FXML
	private Button dodajBtn;
	@FXML
	private ChoiceBox<DrzavaInfo> drzavaList;
	@FXML
	private ListView<JezikDTO> jeziciList;
	@FXML
	private TextField nazivFld;
	@FXML
	private TextField godinaFld;
	@FXML
	private TextField budzetFld;
	@FXML
	private ListView<ZanrDTO> zanroviList;
	@FXML
	private RadioButton pripadaSerijalu;
	@FXML
	private RadioButton nePripadaSerijalu;
	@FXML
	private RadioButton noviSerijal;
	@FXML
	private TextField serijalFld;
	@FXML
	private ChoiceBox<Serijal> serijaliList;
	private ToggleGroup choose;
	private BooleanBinding bind;
	@FXML
	private TextField trajanjeFld;

	// Event Listener on Button[#dodajBtn].onAction
	@FXML
	public void dodaj(ActionEvent event) {
		try {
			BigDecimal budzet = new BigDecimal(budzetFld.getText());
			Integer godina = new Integer(godinaFld.getText());
			Integer trajanje=new Integer(trajanjeFld.getText());
			String nazivSerijala=noviSerijal.selectedProperty().get()?serijalFld.getText():null;
			if (pripadaSerijalu.selectedProperty().get())
				nazivSerijala=serijaliList.getSelectionModel().getSelectedItem().getNazivSerijala();
			Integer id = FilmDAO.dodajFilm(nazivFld.getText(), godina,//serijaliList.getSelectionModel().getSelectedItem().getIdSerijala()
					drzavaList.getSelectionModel().getSelectedItem().getIdDrzave(), budzet,trajanje,nePripadaSerijalu.selectedProperty().not().get(),
					pripadaSerijalu.selectedProperty().get(),nazivSerijala);
			if (id!=0) {
			for (ZanrDTO zanr : zanroviList.getSelectionModel().getSelectedItems())
				ZanrDAO.dodajZanr(id, zanr.getIdZanra());
			for (JezikDTO jezik : jeziciList.getSelectionModel().getSelectedItems())
				JezikDAO.dodajJezik(id, jezik.getIdJezika());
			}
			primaryStage.close();
		} catch (NumberFormatException e) {
			new Alert(AlertType.ERROR, "ERROR").showAndWait();
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		choose = new ToggleGroup();

		(pripadaSerijalu.selectedProperty().and(serijaliList.getSelectionModel().selectedItemProperty().isNull()))
				.or(noviSerijal.selectedProperty().and(serijalFld.textProperty().isEmpty()));
		pripadaSerijalu.setToggleGroup(choose);
		nePripadaSerijalu.setToggleGroup(choose);
		nePripadaSerijalu.setSelected(true);
		noviSerijal.setToggleGroup(choose);
		serijaliList.disableProperty().bind(pripadaSerijalu.selectedProperty().not());
		serijalFld.disableProperty().bind(noviSerijal.selectedProperty().not());
		jeziciList.setItems(JezikDAO.jezici());
		jeziciList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		zanroviList.setItems(ZanrDAO.zanrovi());
		zanroviList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		serijaliList.setItems(Serijal.serijali());
		drzavaList.setItems(DrzavaInfo.drzave());
		dodajBtn.disableProperty().bind(nazivFld.textProperty().isEmpty().or(godinaFld.textProperty().isEmpty()
				.or(budzetFld.textProperty().isEmpty())
				.or(drzavaList.getSelectionModel().selectedItemProperty().isNull().or(trajanjeFld.textProperty().isEmpty()
						.or(zanroviList.getSelectionModel().selectedItemProperty().isNull()
								.or(jeziciList.getSelectionModel().selectedItemProperty().isNull()
										.or((pripadaSerijalu.selectedProperty()
												.and(serijaliList.getSelectionModel().selectedItemProperty().isNull()))
														.or(noviSerijal.selectedProperty()
																.and(serijalFld.textProperty().isEmpty())))))))));

	}

}
