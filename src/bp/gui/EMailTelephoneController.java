package bp.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import java.net.URL;
import java.util.ResourceBundle;

import bp.dao.EMailDAO;
import bp.dao.KompanijaDAO;
import bp.dao.TelefonDAO;
import bp.dto.EMailDTO;
import bp.dto.KompanijaDTO;
import bp.dto.TelefonDTO;
import javafx.event.ActionEvent;

import javafx.scene.control.TableView;

import javafx.scene.control.TableColumn;

public class EMailTelephoneController extends BaseController implements Initializable {
	@FXML
	private TableView<TelefonDTO> telefonTable;
	@FXML
	private TableColumn<TelefonDTO, String> telefonCln;
	@FXML
	private TableView<EMailDTO> mailTable;
	@FXML
	private TableColumn<EMailDTO, String> mailCln;
	@FXML
	private TextField telefon;
	@FXML
	private Button telefonBtn;
	@FXML
	private TextField mail;
	@FXML
	private Button mailBtn;
	@FXML
	private Button brisiMail;
	@FXML
	private Button brisiTel;
	public Integer idKompanije;

	// Event Listener on Button[#telefonBtn].onAction
	@FXML
	public void dodajTelefon(ActionEvent event) {
		TelefonDAO.dodajTelefonKompanije(idKompanije, telefon.getText());
		telefonTable.setItems(TelefonDAO.telefoniKompanije(idKompanije));
	}

	// Event Listener on Button[#mailBtn].onAction
	@FXML
	public void dodajMail(ActionEvent event) {
		EMailDAO.dodajEMailKompanije(idKompanije, mail.getText());
		mailTable.setItems(EMailDAO.emailiKompanije(idKompanije));
	}

	public void brisiEMail() {
		EMailDAO.obrisiEMail(mailTable.getSelectionModel().getSelectedItem().getMail());
		mailTable.setItems(EMailDAO.emailiKompanije(idKompanije));
	}

	public void brisiTelefon() {
		TelefonDAO.obrisiTelefon(telefonTable.getSelectionModel().getSelectedItem().getTelefon());
		telefonTable.setItems(TelefonDAO.telefoniKompanije(idKompanije));
	}

	public void init() {
		brisiTel.disableProperty().bind(telefonTable.getSelectionModel().selectedItemProperty().isNull());
		brisiMail.disableProperty().bind(mailTable.getSelectionModel().selectedItemProperty().isNull());
		mailBtn.disableProperty().bind(mail.textProperty().isEmpty());
		telefonBtn.disableProperty().bind(telefon.textProperty().isEmpty());
		telefonCln.setCellValueFactory(new PropertyValueFactory<>("telefon"));
		mailCln.setCellValueFactory(new PropertyValueFactory<>("mail"));
		telefonCln.setCellFactory(TextFieldTableCell.<TelefonDTO>forTableColumn());
		mailCln.setCellFactory(TextFieldTableCell.<EMailDTO>forTableColumn());
		telefonCln.setOnEditCommit(t -> {
			((TelefonDTO) t.getTableView().getItems().get(t.getTablePosition().getRow())).setTelefon(t.getNewValue());
			TelefonDAO.azurirajTelefon(t.getNewValue());
		});
		mailCln.setOnEditCommit(t -> {
			((EMailDTO) t.getTableView().getItems().get(t.getTablePosition().getRow())).setMail(t.getNewValue());
			EMailDAO.azurirajEMail(t.getNewValue());
		});
		mailTable.setItems(EMailDAO.emailiKompanije(idKompanije));
		telefonTable.setItems(TelefonDAO.telefoniKompanije(idKompanije));
		mailTable.setEditable(true);
		telefonTable.setEditable(true);

	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
}
