package bp.gui;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;

import com.sun.javafx.scene.control.SelectedCellsMap;

import bp.dao.JezikDAO;
import bp.dao.KompanijaDAO;
import bp.dao.OsobaDAO;
import bp.dao.ZanrDAO;
import bp.dto.JezikDTO;
import bp.dto.KompanijaDTO;
import bp.dto.OsobaDTO;
import bp.dto.ZanrDTO;
import bp.info.FilmInfo;
import bp.info.SerijaInfo;
import bp.util.DatePickerTableCell;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

public class WelcomeScreenController extends BaseController implements Initializable {
	@FXML
	private Tab filmTab;
	private boolean filmOpened;
	@FXML
	private TableView<FilmInfo> filmTable;
	@FXML
	private TableColumn<FilmInfo, String> nazivFilmCln;
	@FXML
	private TableColumn<FilmInfo, Integer> godinaFilmCln;
	@FXML
	private TableColumn<FilmInfo, String> drzavaFilmCln;
	@FXML
	private TableColumn<FilmInfo, Integer> trajanjeCln;
	@FXML
	private TableColumn<FilmInfo, String> serijalCln;
	@FXML
	private TableColumn<FilmInfo, Double> budzetFilmCln;
	@FXML
	private Tab serijaTab;
	private boolean serijaOpened;
	@FXML
	private TableView<SerijaInfo> serijaTable;
	@FXML
	private TableColumn<SerijaInfo, String> nazivSerCln;
	@FXML
	private TableColumn<SerijaInfo, Integer> godinaSerCln;
	@FXML
	private TableColumn<SerijaInfo, String> drzavaSerCln;
	@FXML
	private TableColumn<SerijaInfo, Integer> brSezonaCln;
	@FXML
	private TableColumn<SerijaInfo, Integer> brEpizodaCln;
	@FXML
	private TableColumn<SerijaInfo, Double> budzetSerCln;
	@FXML
	private Tab osobaTab;
	private boolean osobaOpened;
	@FXML
	private TableView<OsobaDTO> osobaTable;
	@FXML
	private TableColumn<OsobaDTO, String> imeCln;
	@FXML
	private TableColumn<OsobaDTO, String> prezimeCln;
	@FXML
	private TableColumn<OsobaDTO, LocalDate> datumCln;
	@FXML
	private Tab kompanijaTab;
	private boolean kompanijaOpened;
	@FXML
	private TableView<KompanijaDTO> kompanijaTable;
	@FXML
	private TableColumn<KompanijaDTO, String> nazivKompanijeCln;
	@FXML
	private TableColumn<KompanijaDTO, String> adresaCln;
	@FXML
	private Button dodajKompanijuBtn;
	@FXML
	private TextField nazivFld;
	boolean explicitDraw;
	@FXML
	private TextArea adresaFld;

	@FXML
	private TextField imeFld;
	@FXML
	private TextField prezimeFld;
	@FXML
	private DatePicker datumPick;
	@FXML
	private Button dodajOsobuBtn;
	@FXML
	private Button pogledajPodatke;
	@FXML
	private ListView<JezikDTO> jezikSerije;
	@FXML
	private ListView<JezikDTO> jezikFilma;
	@FXML
	private ListView<ZanrDTO> zanrSerije;
	@FXML
	private ListView<ZanrDTO> zanrFilma;
	public void pogledajPodatkeKompanije() throws IOException {
		Stage addStage=new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("EMailTelephone.fxml"));
		AnchorPane root = (AnchorPane) loader.load();
		EMailTelephoneController control = loader.<EMailTelephoneController>getController();
		control.setPrimaryStage(addStage);
		control.idKompanije=kompanijaTable.getSelectionModel().getSelectedItem().getIdKompanije();
		control.init();
		Scene scene=new Scene(root);
		addStage.initModality(Modality.WINDOW_MODAL);
		addStage.initOwner(primaryStage);
		addStage.setScene(scene);
		addStage.showAndWait();
	}
	
	public void dodajSeriju() throws IOException {
		Stage addStage=new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("AddTVShowScreen.fxml"));
		AnchorPane root = (AnchorPane) loader.load();
		BaseController control = loader.<BaseController>getController();
		control.setPrimaryStage(addStage);
		Scene scene=new Scene(root);
		addStage.initModality(Modality.WINDOW_MODAL);
		addStage.initOwner(primaryStage);
		addStage.setScene(scene);
		addStage.showAndWait();
		explicitDraw=true;
		populateTV();
		
	}
	public void dodajFilm() throws IOException {
		Stage addStage=new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("AddMovieScreen.fxml"));
		AnchorPane root = (AnchorPane) loader.load();
		BaseController control = loader.<BaseController>getController();
		control.setPrimaryStage(addStage);
		Scene scene=new Scene(root);
		addStage.initModality(Modality.WINDOW_MODAL);
		addStage.initOwner(primaryStage);
		addStage.setScene(scene);
		addStage.showAndWait();
		explicitDraw=true;
		populateMovies();
		
	}
	public void populateCompanies() {
		if (explicitDraw || (kompanijaOpened = !kompanijaOpened)) {
			kompanijaTable.setItems(KompanijaDAO.kompanije());
			explicitDraw = false;
		}
	}

	public void populatePersons() {
		if (explicitDraw || (osobaOpened = !osobaOpened)) {
			osobaTable.setItems(OsobaDAO.osobe());
			explicitDraw = false;
		}
	}

	public void populateMovies() {
		if (explicitDraw || (filmOpened = !filmOpened)) {
			filmTable.setItems(FilmInfo.filmovi());
			explicitDraw = false;
		}
	}

	public void populateTV() {
		if (explicitDraw || (serijaOpened = !serijaOpened)) {
			serijaTable.setItems(SerijaInfo.serije());
			explicitDraw = false;
		}
	}

	public void dodajKompaniju() {
		KompanijaDAO.dodajKompaniju(new KompanijaDTO(0, nazivFld.getText(), adresaFld.getText()));
		nazivFld.setText("");
		adresaFld.setText("");
		explicitDraw = true;
		populateCompanies();

	}

	public void dodajOsobu() {
		OsobaDAO.dodajOsobu(new OsobaDTO(0, imeFld.getText(), prezimeFld.getText(), datumPick.getValue()));
		imeFld.setText("");
		prezimeFld.setText("");
		datumPick.setValue(null);
		explicitDraw = true;
		populatePersons();
	}

	public void initializeCompanies() {
		pogledajPodatke.disableProperty().bind(kompanijaTable.getSelectionModel().selectedItemProperty().isNull());
		kompanijaOpened = false;
		kompanijaTable.setEditable(true);
		adresaCln.setCellValueFactory(new PropertyValueFactory<>("adresa"));
		adresaCln.setCellFactory(TextFieldTableCell.<KompanijaDTO>forTableColumn());
		adresaCln.setOnEditCommit(t -> {
			((KompanijaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow())).setAdresa(t.getNewValue());
			KompanijaDAO
					.azurirajKompaniju((KompanijaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow()));
		});
		nazivKompanijeCln.setCellValueFactory(new PropertyValueFactory<>("nazivKompanije"));
		nazivKompanijeCln.setCellFactory(TextFieldTableCell.<KompanijaDTO>forTableColumn());
		nazivKompanijeCln.setOnEditCommit(t -> {
			((KompanijaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow()))
					.setNazivKompanije(t.getNewValue());
			KompanijaDAO
					.azurirajKompaniju((KompanijaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow()));
		});
	}

	public void initializePersons() {
		osobaOpened = false;
		osobaTable.setEditable(true);
		imeCln.setCellValueFactory(new PropertyValueFactory<>("ime"));
		imeCln.setCellFactory(TextFieldTableCell.<OsobaDTO>forTableColumn());
		imeCln.setOnEditCommit(t -> {
			((OsobaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow())).setIme(t.getNewValue());
			OsobaDAO.azurirajOsobu((OsobaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow()));
		});
		prezimeCln.setCellValueFactory(new PropertyValueFactory<>("prezime"));
		prezimeCln.setCellFactory(TextFieldTableCell.<OsobaDTO>forTableColumn());
		prezimeCln.setOnEditCommit(t -> {
			((OsobaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow())).setPrezime(t.getNewValue());
			OsobaDAO.azurirajOsobu((OsobaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow()));
		});

		datumCln.setCellValueFactory(new PropertyValueFactory<>("datumRodjenja"));
		datumCln.setCellFactory(DatePickerTableCell.<OsobaDTO>forTableColumn());
		datumCln.setOnEditCommit(t -> {
			((OsobaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow()))
					.setDatumRodjenja(t.getNewValue());
			OsobaDAO.azurirajOsobu((OsobaDTO) t.getTableView().getItems().get(t.getTablePosition().getRow()));
		});
	}

	public void initializeMovies() {
		filmOpened = false;
		filmTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<FilmInfo>() {

			@Override
			public void changed(ObservableValue<? extends FilmInfo> observable, FilmInfo oldValue, FilmInfo newValue) {
				if (filmTable.getSelectionModel().getSelectedItem()!=null) {
					zanrFilma.setItems(ZanrDAO.zanroviDjela(filmTable.getSelectionModel().getSelectedItem().getIdFilma()));
					jezikFilma.setItems(JezikDAO.jeziciDjela(filmTable.getSelectionModel().getSelectedItem().getIdFilma()));
				}else {
					zanrFilma.setItems(null);
					jezikFilma.setItems(null);
				}
				
			}
		});
		nazivFilmCln.setCellValueFactory(new PropertyValueFactory<>("nazivFilma"));
		drzavaFilmCln.setCellValueFactory(new PropertyValueFactory<>("drzavaPorijekla"));
		godinaFilmCln.setCellValueFactory(new PropertyValueFactory<>("godina"));
		serijalCln.setCellValueFactory(new PropertyValueFactory<>("serijal"));
		trajanjeCln.setCellValueFactory(new PropertyValueFactory<>("trajanje"));
		budzetFilmCln.setCellValueFactory(new PropertyValueFactory<>("budzet"));
	}

	public void initializeTV() {
		serijaOpened = false;
		serijaTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SerijaInfo>() {

			@Override
			public void changed(ObservableValue<? extends SerijaInfo> observable, SerijaInfo oldValue, SerijaInfo newValue) {
				if (serijaTable.getSelectionModel().getSelectedItem()!=null) {
					zanrSerije.setItems(ZanrDAO.zanroviDjela(serijaTable.getSelectionModel().getSelectedItem().getIdSerije()));
					jezikSerije.setItems(JezikDAO.jeziciDjela(serijaTable.getSelectionModel().getSelectedItem().getIdSerije()));
				}else {
					zanrSerije.setItems(null);
					jezikSerije.setItems(null);
				}
				
			}
		});
		nazivSerCln.setCellValueFactory(new PropertyValueFactory<>("nazivSerije"));
		drzavaSerCln.setCellValueFactory(new PropertyValueFactory<>("drzavaPorijekla"));
		godinaSerCln.setCellValueFactory(new PropertyValueFactory<>("godina"));
		brEpizodaCln.setCellValueFactory(new PropertyValueFactory<>("brojEpizoda"));
		brSezonaCln.setCellValueFactory(new PropertyValueFactory<>("brojSezona"));
		budzetSerCln.setCellValueFactory(new PropertyValueFactory<>("budzet"));
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		explicitDraw = false;
		initializeCompanies();
		initializePersons();
		initializeMovies();
		initializeTV();
		detaljiSerija.disableProperty().bind(serijaTable.getSelectionModel().selectedItemProperty().isNull());
		detaljiFilm.disableProperty().bind(filmTable.getSelectionModel().selectedItemProperty().isNull());
		dodajKompanijuBtn.disableProperty()
				.bind(nazivFld.textProperty().isEmpty().or(adresaFld.textProperty().isEmpty()));
		//
		dodajOsobuBtn.disableProperty().bind(imeFld.textProperty().isEmpty()
				.or(prezimeFld.textProperty().isEmpty().or(datumPick.valueProperty().isNull())));
	}
	@FXML
    private Button detaljiSerija;
	@FXML
    private Button detaljiFilm;
	 @FXML
	    void idiNaDetaljeFilma() throws IOException {
		 Stage addStage=new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ViewData.fxml"));
			TabPane root = (TabPane) loader.load();
			ViewDataController control = loader.<ViewDataController>getController();
			control.idDjela=filmTable.getSelectionModel().getSelectedItem().getIdFilma();
			control.serija=false;
			control.populateUloge();
			control.disableTab();
			control.setPrimaryStage(addStage);
			Scene scene=new Scene(root);
			addStage.initModality(Modality.WINDOW_MODAL);
			addStage.initOwner(primaryStage);
			addStage.setScene(scene);
			addStage.showAndWait();
			explicitDraw=true;
			populateMovies();
	    }
	 @FXML
	    void idiNaDetaljeSerije() throws IOException {
		 Stage addStage=new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ViewData.fxml"));
			TabPane root = (TabPane) loader.load();
			ViewDataController control = loader.<ViewDataController>getController();
			control.idDjela=serijaTable.getSelectionModel().getSelectedItem().getIdSerije();
			control.serija=true;
			control.populateUloge();
			control.setPrimaryStage(addStage);
			Scene scene=new Scene(root);
			addStage.initModality(Modality.WINDOW_MODAL);
			addStage.initOwner(primaryStage);
			addStage.setScene(scene);
			addStage.showAndWait();
			explicitDraw=true;
			populateTV();
	    }
}
