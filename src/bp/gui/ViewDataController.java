package bp.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.Date;
import java.text.spi.NumberFormatProvider;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import bp.dao.DistributerDAO;
import bp.dao.EpizodaDAO;
import bp.dao.KompanijaDAO;
import bp.dao.OsobaDAO;
import bp.dao.SezonaDAO;
import bp.dao.UlogaDAO;
import bp.dto.DistributerDTO;
import bp.dto.EpizodaDTO;
import bp.dto.KompanijaDTO;
import bp.dto.OsobaDTO;
import bp.dto.SezonaDTO;
import bp.dto.UlogaDTO;
import bp.info.DrzavaInfo;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.scene.control.ListView;

import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import javafx.scene.control.DatePicker;

import javafx.scene.control.TableView;

import javafx.scene.control.ChoiceBox;

import javafx.scene.control.TableColumn;

public class ViewDataController extends BaseController implements Initializable {
	@FXML
	private TableView<UlogaDTO> ulogaTable;
	@FXML
	private TableColumn<UlogaDTO, String> ulogaCln;
	@FXML
	private TableColumn<UlogaDTO, String> imeCln;
	@FXML
	private TableColumn<UlogaDTO, String> prezimeCln;
	@FXML
	private TableColumn<UlogaDTO, LocalDate> datumRodjenjaCln;
	@FXML
	private TableColumn<UlogaDTO, LocalDate> potpisUlogaCln;
	@FXML
	private TableColumn<UlogaDTO, LocalDate> pocetakUlogaCln;
	@FXML
	private TableColumn<UlogaDTO, LocalDate> krajUlogaCln;
	@FXML
	private TableColumn<UlogaDTO, String> honorarCln;
	@FXML
	private ChoiceBox<OsobaDTO> osobaList;
	@FXML
	private TextField novaUlogaFld;
	@FXML
	private DatePicker potpisUlogaPicker;
	@FXML
	private DatePicker krajUlogaPicker;
	@FXML
	private DatePicker pocetakUlogaPicker;
	@FXML
	private TextField honorarFld;
	@FXML
	private Button dodajUloguBtn;
	// SERIJA
	@FXML
	private Tab serijaTab;
	@FXML
	private TableView<EpizodaDTO> epizodaTable;
	@FXML
	private TableColumn<EpizodaDTO, Integer> brEpizodeCln;
	@FXML
	private TableColumn<EpizodaDTO, String> nazivEpCln;
	@FXML
	private TableColumn<EpizodaDTO, Integer> trajanjeCln;
	@FXML
	private TableView<SezonaDTO> sezonaTable;
	@FXML
	private TableColumn<SezonaDTO, Integer> brSezoneCln;
	@FXML
	private TableColumn<SezonaDTO, Integer> brEpuSezCln;
	@FXML
	private Spinner<Integer> sezonaSpin;
	@FXML
	private Button dodajSezonuBtn;
	@FXML
	private Spinner<Integer> novaEpSpin;
	@FXML
	private TextField trajanjeFld;
	@FXML
	private TextField nazivEpFld;
	@FXML
	private Button dodajEpizoduBtn;
	// TAB DISTRIBUTER
	@FXML
	private TableView<DistributerDTO> distributeriTable;
	@FXML
	private TableColumn<DistributerDTO, String> kompanijaCln;
	@FXML
	private TableColumn<DistributerDTO, LocalDate> potpisDistrCln;
	@FXML
	private TableColumn<DistributerDTO, LocalDate> pocetakDistCln;
	@FXML
	private TableColumn<DistributerDTO, LocalDate> krajDistCln;
	@FXML
	private ChoiceBox<KompanijaDTO> kompanijeList;
	@FXML
	private Button dodajDistributeraBtn;
	@FXML
	private DatePicker pocetakDistPicker;
	@FXML
	private DatePicker krajDistPicker;
	@FXML
	private DatePicker potpisDistPicker;
	@FXML
	private ListView<DrzavaInfo> drzaveDistrList;
	@FXML
	private ChoiceBox<DrzavaInfo> drzavaList;
	@FXML
	private Button dodajDrzavuBtn;
	// OSTALO
	public boolean serija;
	public Integer idDjela;
	private boolean explicit;
	private boolean serijaOpened;
	private boolean ulogaOpened;
	private boolean distributerOpened;

	// Event Listener on Button[#dodajUloguBtn].onAction
	@FXML
	public void dodajUlogu(ActionEvent event) {
		try {
			BigDecimal decimal = new BigDecimal(honorarFld.getText());

			UlogaDAO.dodajUlogu(osobaList.getSelectionModel().getSelectedItem().getIdOsobe(), novaUlogaFld.getText(),
					idDjela, decimal, potpisUlogaPicker.getValue(), pocetakUlogaPicker.getValue(),
					krajUlogaPicker.getValue());
			explicit = true;
			populateUloge();
		} catch (NumberFormatException e) {
			new Alert(AlertType.ERROR, "ERROR").showAndWait();
		}
	}

	// Event Listener on Button[#dodajSezonuBtn].onAction
	@FXML
	public void dodajSezonu(ActionEvent event) {
		SezonaDAO.dodajSezonu(idDjela, sezonaSpin.getValue());
		explicit=true;
		populateSerija();
	}

	// Event Listener on Button[#dodajEpizoduBtn].onAction
	@FXML
	public void dodajEpizodu(ActionEvent event) {
		try {
			Integer trajanje = new Integer(trajanjeFld.getText());
			EpizodaDAO.dodajEpizodu(idDjela, sezonaTable.getSelectionModel().getSelectedItem().getBrojSezone(),
					novaEpSpin.getValue(), nazivEpFld.getText(), trajanje);
			explicit = true;
			populateSerija();
		} catch (NumberFormatException e) {
			new Alert(AlertType.ERROR, "ERROR").showAndWait();
		}
	}

	// Event Listener on Button[#dodajDistributeraBtn].onAction
	@FXML
	public void dodajDistributera(ActionEvent event) {
		DistributerDAO.dodajDistributera(kompanijeList.getSelectionModel().getSelectedItem().getIdKompanije(), idDjela,
				potpisDistPicker.getValue(), pocetakDistPicker.getValue(), krajDistPicker.getValue());
		explicit = true;
		populateDistributer();
	}

	// Event Listener on Button[#dodajDrzavuBtn].onAction
	@FXML
	public void dodajDrzavu(ActionEvent event) {
		DistributerDAO.dodajDrzavuDistribucije(drzavaList.getSelectionModel().getSelectedItem().getIdDrzave(),
				distributeriTable.getSelectionModel().getSelectedItem().getIdUgovora());
		drzaveDistrList.setItems(DistributerDAO
				.drzaveDistribucije(distributeriTable.getSelectionModel().getSelectedItem().getIdUgovora()));
		ObservableList<DrzavaInfo> drzave = DrzavaInfo.drzave();
		drzave.removeAll(drzaveDistrList.getItems());
		drzavaList.setItems(drzave);
	}

	// POMOCNE FUNKCIJE
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initUlogeTab();
		initDistributeriTab();
		initSerijaTab();

	}

	public void populateUloge() {
		if (explicit || (ulogaOpened = !ulogaOpened)) {
			if (idDjela != null)
				ulogaTable.setItems(UlogaDAO.epizode(idDjela));
			osobaList.setItems(OsobaDAO.osobe());
			explicit = false;
		}
	}

	public void initUlogeTab() {
		ulogaOpened = false;
		ulogaCln.setCellValueFactory(new PropertyValueFactory<>("nazivUloge"));
		imeCln.setCellValueFactory(new PropertyValueFactory<>("ime"));
		prezimeCln.setCellValueFactory(new PropertyValueFactory<>("prezime"));
		datumRodjenjaCln.setCellValueFactory(new PropertyValueFactory<>("datumRodjenja"));
		potpisUlogaCln.setCellValueFactory(new PropertyValueFactory<>("datumPotpisivanja"));
		pocetakUlogaCln.setCellValueFactory(new PropertyValueFactory<>("datumOd"));
		krajUlogaCln.setCellValueFactory(new PropertyValueFactory<>("datumDo"));
		honorarCln.setCellValueFactory(new PropertyValueFactory<>("honorar"));
		dodajUloguBtn.disableProperty()
				.bind(novaUlogaFld.textProperty().isEmpty()
						.or(honorarFld.textProperty().isEmpty()
								.or(pocetakUlogaPicker.valueProperty().isNull()
										.or(krajUlogaPicker.valueProperty().isNull().or(potpisUlogaPicker
												.valueProperty().isNull()
												.or(osobaList.getSelectionModel().selectedItemProperty().isNull()))))));

	}

	public void populateDistributer() {
		if (explicit || (distributerOpened = !distributerOpened)) {
			distributeriTable.setItems(DistributerDAO.distributeri(idDjela));
			kompanijeList.setItems(KompanijaDAO.kompanije());
		}
	}

	public void initDistributeriTab() {
		distributerOpened = false;
		kompanijaCln.setCellValueFactory(new PropertyValueFactory<>("nazivKompanije"));
		potpisDistrCln.setCellValueFactory(new PropertyValueFactory<>("datumPotpisivanja"));
		pocetakDistCln.setCellValueFactory(new PropertyValueFactory<>("datumOd"));
		krajDistCln.setCellValueFactory(new PropertyValueFactory<>("datumDo"));
		dodajDrzavuBtn.disableProperty().bind(drzavaList.getSelectionModel().selectedItemProperty().isNull());
		drzavaList.disableProperty().bind(distributeriTable.getSelectionModel().selectedItemProperty().isNull());
		dodajDistributeraBtn.disableProperty()
				.bind(kompanijeList.getSelectionModel().selectedItemProperty().isNull().or(potpisDistPicker
						.valueProperty().isNull()
						.or(pocetakDistPicker.valueProperty().isNull().or(krajDistPicker.valueProperty().isNull()))));
		distributeriTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<DistributerDTO>() {

			@Override
			public void changed(ObservableValue<? extends DistributerDTO> observable, DistributerDTO oldValue,
					DistributerDTO newValue) {
				if (distributeriTable.getSelectionModel().getSelectedItem() != null) {
					drzaveDistrList.setItems(DistributerDAO.drzaveDistribucije(
							distributeriTable.getSelectionModel().getSelectedItem().getIdUgovora()));
					ObservableList<DrzavaInfo> drzave = DrzavaInfo.drzave();
					drzave.removeAll(drzaveDistrList.getItems());
					drzavaList.setItems(drzave);
				}

			}
		});
	}

	public void initSerijaTab() {
		serijaOpened = false;

		sezonaSpin.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1000, 1));
		novaEpSpin.setValueFactory((new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1000, 1)));
		brSezoneCln.setCellValueFactory(new PropertyValueFactory<>("brojSezone"));
		brEpuSezCln.setCellValueFactory(new PropertyValueFactory<>("brojEpizodaUSezoni"));
		brEpizodeCln.setCellValueFactory(new PropertyValueFactory<>("brojEpizode"));
		nazivEpCln.setCellValueFactory(new PropertyValueFactory<>("nazivEpizode"));
		trajanjeCln.setCellValueFactory(new PropertyValueFactory<>("trajanjeEpizode"));
		dodajSezonuBtn.disableProperty().bind(sezonaSpin.valueProperty().isNull());
		sezonaTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SezonaDTO>() {

			@Override
			public void changed(ObservableValue<? extends SezonaDTO> observable, SezonaDTO oldValue,
					SezonaDTO newValue) {
				if (sezonaTable.getSelectionModel().getSelectedItem() != null) {
					epizodaTable.setItems(EpizodaDAO.epizode(idDjela,
							sezonaTable.getSelectionModel().getSelectedItem().getBrojSezone()));
				}

			}
		});
		dodajEpizoduBtn.disableProperty()
				.bind(nazivEpFld.textProperty().isEmpty()
						.or(trajanjeFld.textProperty().isEmpty().or(novaEpSpin.valueProperty().isNull()
								.or(sezonaTable.getSelectionModel().selectedItemProperty().isNull()))));
	}

	public void populateSerija() {
		if (explicit || (serijaOpened = !serijaOpened)) {
			sezonaTable.setItems(SezonaDAO.sezone(idDjela));
			epizodaTable.setItems(null);
			explicit = false;
		}
	}

	public void disableTab() {
		if (!serija) {
			serijaTab.setDisable(true);
		}

	}

}
