package bp.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxListCell;

import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.ClosedDirectoryStreamException;
import java.text.ParseException;
import java.util.ResourceBundle;

import bp.dao.JezikDAO;
import bp.dao.SerijaDAO;
import bp.dao.ZanrDAO;
import bp.dto.JezikDTO;
import bp.dto.ZanrDTO;
import bp.info.DrzavaInfo;
import javafx.event.ActionEvent;

import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.ChoiceBox;

public class AddTVShowScreenController extends BaseController implements Initializable {
	@FXML
	private Button dodajBtn;
	@FXML
	private ChoiceBox<DrzavaInfo> drzavaList;
	@FXML
	private ListView<JezikDTO> jeziciList;
	@FXML
	private TextField nazivFld;
	@FXML
	private TextField godinaFld;
	@FXML
	private TextField budzetFld;
	@FXML
	private ListView<ZanrDTO> zanroviList;

	// Event Listener on Button[#dodajBtn].onAction
	@FXML
	public void dodaj(ActionEvent event) {
		try {
		BigDecimal budzet=new BigDecimal(budzetFld.getText());
		Integer godina=new Integer(godinaFld.getText());
		Integer id=SerijaDAO.dodajSeriju(nazivFld.getText(), godina, drzavaList.getSelectionModel().getSelectedItem().getIdDrzave(), budzet);
		if (id!=0) {

		for (ZanrDTO zanr:zanroviList.getSelectionModel().getSelectedItems())
			ZanrDAO.dodajZanr(id, zanr.getIdZanra());
		for (JezikDTO jezik:jeziciList.getSelectionModel().getSelectedItems())
			JezikDAO.dodajJezik(id, jezik.getIdJezika());
		}
		primaryStage.close();
		}catch (NumberFormatException e) {
			new Alert(AlertType.ERROR,"ERROR").showAndWait();
		}
		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		jeziciList.setItems(JezikDAO.jezici());
		jeziciList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		zanroviList.setItems(ZanrDAO.zanrovi());
		zanroviList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		drzavaList.setItems(DrzavaInfo.drzave());
		dodajBtn.disableProperty()
				.bind(nazivFld.textProperty().isEmpty()
						.or(godinaFld.textProperty().isEmpty().or(budzetFld.textProperty().isEmpty())
								.or(drzavaList.getSelectionModel().selectedItemProperty().isNull()
										.or(zanroviList.getSelectionModel().selectedItemProperty().isNull()
												.or(jeziciList.getSelectionModel().selectedItemProperty().isNull())))));

	}
}
