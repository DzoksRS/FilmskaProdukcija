package bp.gui;
import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class BaseController {

	protected Stage primaryStage;

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public static BaseController changeScene(String path, Stage primaryStage) throws IOException {
		FXMLLoader loader = new FXMLLoader(BaseController.class.getResource(path));
		AnchorPane pane = (AnchorPane) loader.load();
		BaseController control = loader.<BaseController>getController();
		control.setPrimaryStage(primaryStage);
		control.getPrimaryStage().setScene(new Scene(pane));
		control.primaryStage.setOnCloseRequest(event->{});
		return control;
	}
}
