package bp.info;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class TipOsobljaInfo {
	private IntegerProperty idTipa;
	private StringProperty nazivTipa;
	
	public TipOsobljaInfo(Integer idTipa,String nazivTipa) {
		this.idTipa=new SimpleIntegerProperty(idTipa);
		this.nazivTipa=new SimpleStringProperty(nazivTipa);
	}

	public Integer getIdTipa() {
		return idTipa.get();
	}

	public void setIdTipa(Integer idTipa) {
		this.idTipa.set(idTipa);
	}

	public String getNazivTipa() {
		return nazivTipa.get();
	}

	public void setNazivTipa(String nazivTipa) {
		this.nazivTipa.set(nazivTipa);
	}
	
	public String toString() {
		return nazivTipa.get();
	}
	public static ObservableList<TipOsobljaInfo> tipoviOsoblja(){
		ObservableList<TipOsobljaInfo> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM tip_osoblja";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new TipOsobljaInfo(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
}