package bp.info;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Serijal {

	private StringProperty nazivSerijala;
	
	public Serijal(String nazivSerijala) {

		this.nazivSerijala=new SimpleStringProperty(nazivSerijala);
	}

	public String getNazivSerijala() {
		return nazivSerijala.get();
	}

	public void setNazivSerijala(String nazivSerijala) {
		this.nazivSerijala.set(nazivSerijala);
	}
	
	public String toString() {
		return nazivSerijala.get();
	}
	public static ObservableList<Serijal> serijali(){
		ObservableList<Serijal> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT NazivSerijala FROM serijal";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new Serijal(rs.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
}