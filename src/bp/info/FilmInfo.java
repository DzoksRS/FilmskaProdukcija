package bp.info;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class FilmInfo {

	private IntegerProperty idFilma;
	private StringProperty nazivFilma;
	private IntegerProperty godina;
	private StringProperty drzavaPorijekla;
	private IntegerProperty trajanje;
	private StringProperty serijal;
	private StringProperty budzet;
	public FilmInfo(Integer idFilma, String nazivFilma, Integer godina,String drzavaPorijekla, Integer trajanje, String serijal, String budzet) {
		this.idFilma = new SimpleIntegerProperty(idFilma);
		this.nazivFilma = new SimpleStringProperty(nazivFilma);
		this.drzavaPorijekla = new SimpleStringProperty(drzavaPorijekla);
		this.godina = new SimpleIntegerProperty(godina);
		this.trajanje = new SimpleIntegerProperty(trajanje);
		this.serijal = new SimpleStringProperty(serijal==null?"":serijal);
		this.budzet = new SimpleStringProperty(budzet);
	}
	public Integer getIdFilma() {
		return idFilma.get();
	}
	public void setIdFilma(Integer idFilma) {
		this.idFilma.set(idFilma);
	}
	public String getNazivFilma() {
		return nazivFilma.get();
	}
	public void setNazivFilma(String nazivFilma) {
		this.nazivFilma.set(nazivFilma);
	}
	public Integer getGodina() {
		return godina.get();
	}
	public void setGodina(Integer godina) {
		this.godina.set(godina);
	}
	public String getDrzavaPorijekla() {
		return drzavaPorijekla.get();
	}
	public void setDrzavaPorijekla(String drzavaPorijekla) {
		this.drzavaPorijekla.set(drzavaPorijekla);
	}
	public Integer getTrajanje() {
		return trajanje.get();
	}
	public void setTrajanje(Integer trajanje) {
		this.trajanje.set(trajanje);
	}
	public String getSerijal() {
		return serijal.get();
	}
	public void setSerijal(String serijal) {
		this.serijal.set(serijal==null?"":serijal);
	}
	public String getBudzet() {
		return budzet.get();
	}
	public void setBudzet(String budzet) {
		this.budzet.set(budzet);;
	}
	
	public static ObservableList<FilmInfo> filmovi(){
		ObservableList<FilmInfo> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM film_info";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new FilmInfo(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5),
						rs.getString(6), rs.getBigDecimal(7).toString()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
}
