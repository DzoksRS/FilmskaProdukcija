package bp.info;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SerijaInfo {
	private IntegerProperty idSerije;
	private StringProperty nazivSerije;
	private IntegerProperty godina;
	private StringProperty drzavaPorijekla;
	private IntegerProperty brojSezona;
	private IntegerProperty brojEpizoda ;
	private StringProperty budzet;
	public SerijaInfo(Integer idSerije, String nazivSerije, Integer godina,String drzavaPorijekla, Integer brojSezona,Integer brojEpizoda, String budzet) {
		this.idSerije = new SimpleIntegerProperty(idSerije);
		this.nazivSerije = new SimpleStringProperty(nazivSerije);
		this.drzavaPorijekla = new SimpleStringProperty(drzavaPorijekla);
		this.godina = new SimpleIntegerProperty(godina);
		this.brojEpizoda = new SimpleIntegerProperty(brojEpizoda);
		this.brojSezona = new SimpleIntegerProperty(brojSezona);
		this.budzet = new SimpleStringProperty(budzet);
	}
	public Integer getIdSerije() {
		return idSerije.get();
	}
	public void setIdSerije(Integer idSerije) {
		this.idSerije.set(idSerije);
	}
	public String getNazivSerije() {
		return nazivSerije.get();
	}
	public void setNazivSerije(String nazivSerije) {
		this.nazivSerije.set(nazivSerije);
	}
	public Integer getGodina() {
		return godina.get();
	}
	public void setGodina(Integer godina) {
		this.godina.set(godina);
	}
	public String getDrzavaPorijekla() {
		return drzavaPorijekla.get();
	}
	public void setDrzavaPorijekla(String drzavaPorijekla) {
		this.drzavaPorijekla.set(drzavaPorijekla);
	}
	public Integer getBrojEpizoda() {
		return brojEpizoda.get();
	}
	public void setBrojEpizoda(Integer brojEpizoda) {
		this.brojEpizoda.set(brojEpizoda);
	}
	public Integer getBrojSezona() {
		return brojSezona.get();
	}
	public void setBrojSezona(Integer brojSezona) {
		this.brojSezona.set(brojSezona);
	}
	public String getBudzet() {
		return budzet.get();
	}
	public void setBudzet(String budzet) {
		this.budzet.set(budzet);;
	}
	
	public static ObservableList<SerijaInfo> serije(){
		ObservableList<SerijaInfo> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM serija_info";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new SerijaInfo(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5),
						rs.getInt(6), rs.getBigDecimal(7).toString()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
}
