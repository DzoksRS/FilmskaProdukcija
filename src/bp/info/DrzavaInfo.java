package bp.info;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DrzavaInfo {
	private IntegerProperty idDrzave;
	private StringProperty nazivDrzave;
	
	public DrzavaInfo(Integer idDrzave,String nazivDrzave) {
		this.idDrzave=new SimpleIntegerProperty(idDrzave);
		this.nazivDrzave=new SimpleStringProperty(nazivDrzave);
	}

	public Integer getIdDrzave() {
		return idDrzave.get();
	}

	public void setIdDrzave(Integer idDrzave) {
		this.idDrzave.set(idDrzave);
	}



	@Override
	public boolean equals(Object obj) {
		if (obj==null)
			return false;
		return ((DrzavaInfo)obj).idDrzave.get()==this.idDrzave.get();
	}

	public String getNazivDrzave() {
		return nazivDrzave.get();
	}

	public void setNazivDrzave(String nazivDrzave) {
		this.nazivDrzave.set(nazivDrzave);
	}
	public static ObservableList<DrzavaInfo> drzave(){
		ObservableList<DrzavaInfo> lista = FXCollections.observableArrayList();
		Connection conn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		String query = "SELECT * FROM drzava";
		try {
			conn = ConnectionPool.getInstance().checkOut();
			cs = conn.prepareCall(query);
			rs = cs.executeQuery();
			while (rs.next()) {
				lista.add(new DrzavaInfo(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtilities.getInstance().showSQLException(e);
		} finally {
			ConnectionPool.getInstance().checkIn(conn);
			DBUtilities.getInstance().close(cs, rs);
		}
		return lista;
	}
	
	public String toString() {
		return nazivDrzave.get();
	}
}