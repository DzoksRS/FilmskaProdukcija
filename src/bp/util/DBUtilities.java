package bp.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class DBUtilities {
	private static DBUtilities instance;

	public static DBUtilities getInstance() {
		if (instance == null)
			instance = new DBUtilities();
		return instance;
	}

	private DBUtilities() {
	}

	public void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void close(Statement s) {
		if (s != null) {
			try {
				s.close();
			} catch (SQLException e) {
				
			}
		}
	}

	public void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				
			}
		}
	}

	public void close(Connection conn, Statement s) {
		close(s);
		close(conn);
	}

	public void close(Connection conn, ResultSet rs) {
		close(rs);
		close(conn);
	}

	public void close(Statement s, ResultSet rs) {
		close(rs);
		close(s);
	}

	public void close(Connection conn, Statement s, ResultSet rs) {
		close(rs);
		close(s);
		close(conn);
	}

	public String preparePattern(String text) {
		return text.replace('*', '%').replace('?', '_');
	}

	public void showSQLException(SQLException e) {
		Alert al=new Alert(AlertType.ERROR);
		al.setContentText("Kod greške: " + e.getErrorCode()
				+ "\nPoruka: " + e.getMessage());
		al.setHeaderText("Greška (baza podataka)");
		al.showAndWait();
	}

	public void showErrorMessage(String message) {
		Alert al=new Alert(AlertType.ERROR);
		al.setContentText(message);
		al.setHeaderText("Greška (baza podataka)");
		al.showAndWait();
	}
}
