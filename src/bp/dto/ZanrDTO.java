package bp.dto;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ZanrDTO {
	private IntegerProperty idZanra;
	private StringProperty nazivZanra;
	private BooleanProperty selectedProperty;
	public ZanrDTO(Integer idZanra,String nazivZanra) {
		this.idZanra=new SimpleIntegerProperty(idZanra);
		this.nazivZanra=new SimpleStringProperty(nazivZanra);
		this.selectedProperty=new SimpleBooleanProperty(false);
	}


	public Integer getIdZanra() {
		return idZanra.get();
	}

	public void setIdZanra(Integer idZanra) {
		this.idZanra.set(idZanra);
	}

	public String getNazivZanra() {
		return nazivZanra.get();
	}

	public void setNazivZanra(String nazivZanra) {
		this.nazivZanra.set(nazivZanra);
	}


	public String toString() {
		return nazivZanra.get();
	}

	public BooleanProperty selectedProperty() {
		return selectedProperty;
	}

	public Boolean getSelectedProperty() {
		return selectedProperty.get();
	}

	public void setSelectedProperty(Boolean selectedProperty) {
		this.selectedProperty.set(selectedProperty);
	}
}
