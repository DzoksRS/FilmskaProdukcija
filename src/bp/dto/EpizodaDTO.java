package bp.dto;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EpizodaDTO {
	private IntegerProperty brojSezone;
	private IntegerProperty idDjela;
	private IntegerProperty brojEpizode;
	private StringProperty nazivEpizode;
	private IntegerProperty trajanjeEpizode;
	public EpizodaDTO(Integer brojSezone,Integer idDjela,Integer brojEpizode,String nazivEpizode,Integer trajanjeEpizode) {
		this.brojSezone=new SimpleIntegerProperty(brojSezone);
		this.idDjela=new SimpleIntegerProperty(idDjela);
		this.brojEpizode=new SimpleIntegerProperty(brojEpizode);
		this.nazivEpizode=new SimpleStringProperty(nazivEpizode);
		this.trajanjeEpizode=new SimpleIntegerProperty(trajanjeEpizode);
	}
	public IntegerProperty brojSezoneProperty() {
		return this.brojSezone;
	}
	
	public int getBrojSezone() {
		return this.brojSezoneProperty().get();
	}
	
	public void setBrojSezone(final int brojSezone) {
		this.brojSezoneProperty().set(brojSezone);
	}
	
	public IntegerProperty idDjelaProperty() {
		return this.idDjela;
	}
	
	public int getIdDjela() {
		return this.idDjelaProperty().get();
	}
	
	public void setIdDjela(final int idDjela) {
		this.idDjelaProperty().set(idDjela);
	}
	
	public IntegerProperty brojEpizodeProperty() {
		return this.brojEpizode;
	}
	
	public int getBrojEpizode() {
		return this.brojEpizodeProperty().get();
	}
	
	public void setBrojEpizode(final int brojEpizode) {
		this.brojEpizodeProperty().set(brojEpizode);
	}
	
	public StringProperty nazivEpizodeProperty() {
		return this.nazivEpizode;
	}
	
	public String getNazivEpizode() {
		return this.nazivEpizodeProperty().get();
	}
	
	public void setNazivEpizode(final String nazivEpizode) {
		this.nazivEpizodeProperty().set(nazivEpizode);
	}
	
	public IntegerProperty trajanjeEpizodeProperty() {
		return this.trajanjeEpizode;
	}
	
	public int getTrajanjeEpizode() {
		return this.trajanjeEpizodeProperty().get();
	}
	
	public void setTrajanjeEpizode(final int trajanjeEpizode) {
		this.trajanjeEpizodeProperty().set(trajanjeEpizode);
	}
	
}
