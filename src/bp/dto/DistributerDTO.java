package bp.dto;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DistributerDTO {
	private IntegerProperty idKompanije;
	private IntegerProperty idUgovora;
	private ObjectProperty<LocalDate> datumPotpisivanja;
	private ObjectProperty<LocalDate> datumOd;
	private ObjectProperty<LocalDate> datumDo;
	private IntegerProperty idDjela;
	private StringProperty nazivKompanije;
	private StringProperty adresa;

	public DistributerDTO(Integer idKompanije, Integer idUgovora, LocalDate datumPotpisivanja, LocalDate datumOd,
			LocalDate datumDo, Integer idDjela, String nazivKompanije, String adresa) {
		super();
		this.idKompanije = new SimpleIntegerProperty(idKompanije);
		this.idUgovora = new SimpleIntegerProperty(idUgovora);
		this.datumPotpisivanja = new SimpleObjectProperty<>(datumPotpisivanja);
		this.datumOd = new SimpleObjectProperty<>(datumOd);
		this.datumDo = new SimpleObjectProperty<>(datumDo);
		this.idDjela = new SimpleIntegerProperty(idDjela);
		this.nazivKompanije = new SimpleStringProperty(nazivKompanije);
		this.adresa =  new SimpleStringProperty(adresa);
	}

	public IntegerProperty idKompanijeProperty() {
		return this.idKompanije;
	}

	public int getIdKompanije() {
		return this.idKompanijeProperty().get();
	}

	public void setIdKompanije(final int idKompanije) {
		this.idKompanijeProperty().set(idKompanije);
	}

	public IntegerProperty idUgovoraProperty() {
		return this.idUgovora;
	}

	public int getIdUgovora() {
		return this.idUgovoraProperty().get();
	}

	public void setIdUgovora(final int idUgovora) {
		this.idUgovoraProperty().set(idUgovora);
	}

	public ObjectProperty<LocalDate> datumPotpisivanjaProperty() {
		return this.datumPotpisivanja;
	}

	public LocalDate getDatumPotpisivanja() {
		return this.datumPotpisivanjaProperty().get();
	}

	public void setDatumPotpisivanja(final LocalDate datumPotpisivanja) {
		this.datumPotpisivanjaProperty().set(datumPotpisivanja);
	}

	public ObjectProperty<LocalDate> datumOdProperty() {
		return this.datumOd;
	}

	public LocalDate getDatumOd() {
		return this.datumOdProperty().get();
	}

	public void setDatumOd(final LocalDate datumOd) {
		this.datumOdProperty().set(datumOd);
	}

	public ObjectProperty<LocalDate> datumDoProperty() {
		return this.datumDo;
	}

	public LocalDate getDatumDo() {
		return this.datumDoProperty().get();
	}

	public void setDatumDo(final LocalDate datumDo) {
		this.datumDoProperty().set(datumDo);
	}

	public IntegerProperty idDjelaProperty() {
		return this.idDjela;
	}

	public int getIdDjela() {
		return this.idDjelaProperty().get();
	}

	public void setIdDjela(final int idDjela) {
		this.idDjelaProperty().set(idDjela);
	}

	public StringProperty nazivKompanijeProperty() {
		return this.nazivKompanije;
	}

	public String getNazivKompanije() {
		return this.nazivKompanijeProperty().get();
	}

	public void setNazivKompanije(final String nazivKompanije) {
		this.nazivKompanijeProperty().set(nazivKompanije);
	}

	public StringProperty adresaProperty() {
		return this.adresa;
	}

	public String getAdresa() {
		return this.adresaProperty().get();
	}

	public void setAdresa(final String adresa) {
		this.adresaProperty().set(adresa);
	}

}
