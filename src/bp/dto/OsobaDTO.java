package bp.dto;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OsobaDTO {

	private IntegerProperty idOsobe;
	private StringProperty ime;
	private StringProperty prezime;
	private ObjectProperty<LocalDate> datumRodjenja;

	public Integer getIdOsobe() {
		return idOsobe.get();
	}

	public void setIdOsobe(Integer idOsobe) {
		this.idOsobe.set(idOsobe);;
	}

	public String getIme() {
		return ime.get();
	}

	public void setIme(String ime) {
		this.ime.set(ime);;
	}

	public String getPrezime() {
		return prezime.get();
	}

	public void setPrezime(String prezime) {
		this.prezime.set(prezime);;
	}

	public LocalDate getDatumRodjenja() {
		return datumRodjenja.get();
	}

	public String toString() {
		
		String		a=datumRodjenja.get()==null?"":datumRodjenja.get().toString();
		return ime.get()+" "+prezime.get()+" "+a;
	}
	public void setDatumRodjenja(LocalDate datumRodjenja) {
		this.datumRodjenja.set(datumRodjenja);
	}

	public OsobaDTO(Integer idOsobe, String ime, String prezime, LocalDate datumRodjenja) {
		this.idOsobe = new SimpleIntegerProperty(idOsobe);
		this.ime = new SimpleStringProperty(ime);
		this.prezime = new SimpleStringProperty(prezime);
		
		this.datumRodjenja = new SimpleObjectProperty<LocalDate>(datumRodjenja);
	}


}
