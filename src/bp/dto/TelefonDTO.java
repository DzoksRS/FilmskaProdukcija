package bp.dto;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TelefonDTO {

	StringProperty telefon;
	
	public String getTelefon() {
		return telefon.get();
	}

	public void setTelefon(String telefon) {
		this.telefon.set(telefon);
	}

	public TelefonDTO(String telefon) {
		this.telefon=new SimpleStringProperty(telefon);
	}
}
