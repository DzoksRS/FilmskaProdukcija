package bp.dto;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bp.util.ConnectionPool;
import bp.util.DBUtilities;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class JezikDTO {
	private IntegerProperty idJezika;
	private StringProperty nazivJezika;
	private BooleanProperty selectedProperty;
	public JezikDTO(Integer idJezika,String nazivJezika) {
		this.idJezika=new SimpleIntegerProperty(idJezika);
		this.nazivJezika=new SimpleStringProperty(nazivJezika);
		this.selectedProperty=new SimpleBooleanProperty(false);

	}

	public Integer getIdJezika() {
		return idJezika.get();
	}

	public void setIdJezika(Integer idJezika) {
		this.idJezika.set(idJezika);
	}

	public String getNazivJezika() {
		return nazivJezika.get();
	}

	public void setNazivJezika(String nazivJezika) {
		this.nazivJezika.set(nazivJezika);
	}
	
	
	public BooleanProperty selectedProperty() {
		return selectedProperty;
	}

	public Boolean getSelectedProperty() {
		return selectedProperty.get();
	}

	public void setSelectedProperty(Boolean selectedProperty) {
		this.selectedProperty.set(selectedProperty);
	}
	
	public String toString() {
		return nazivJezika.get();
	}
}