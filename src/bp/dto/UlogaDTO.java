package bp.dto;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UlogaDTO {
	private IntegerProperty idDjela;
	private IntegerProperty idOsobe;
	private StringProperty nazivUloge;
	private StringProperty ime;
	private StringProperty prezime;
	private ObjectProperty<LocalDate> datumRodjenja;
	private ObjectProperty<LocalDate> datumPotpisivanja;
	private ObjectProperty<LocalDate> datumOd;
	private ObjectProperty<LocalDate> datumDo;
	private StringProperty honorar;

	public UlogaDTO(Integer idDjela, Integer idOsobe, String nazivUloge, String ime, String prezime,
			LocalDate datumRodjenja, LocalDate datumPotpisivanja, LocalDate datumOd, LocalDate datumDo,
			String honorar) {
		this.idOsobe=new SimpleIntegerProperty(idOsobe);
		this.nazivUloge=new SimpleStringProperty(nazivUloge);
		this.ime=new SimpleStringProperty(ime);
		this.prezime=new SimpleStringProperty(prezime);
		this.datumRodjenja=new SimpleObjectProperty<LocalDate>(datumRodjenja);
		this.datumPotpisivanja = new SimpleObjectProperty<>(datumPotpisivanja);
		this.datumOd = new SimpleObjectProperty<>(datumOd);
		this.datumDo = new SimpleObjectProperty<>(datumDo);
		this.idDjela = new SimpleIntegerProperty(idDjela);
		this.honorar=new SimpleStringProperty(honorar);
	}

	public IntegerProperty idDjelaProperty() {
		return this.idDjela;
	}
	

	public int getIdDjela() {
		return this.idDjelaProperty().get();
	}
	

	public void setIdDjela(int idDjela) {
		this.idDjelaProperty().set(idDjela);
	}
	

	public IntegerProperty idOsobeProperty() {
		return this.idOsobe;
	}
	

	public int getIdOsobe() {
		return this.idOsobeProperty().get();
	}
	

	public void setIdOsobe(int idOsobe) {
		this.idOsobeProperty().set(idOsobe);
	}
	

	public StringProperty nazivUlogeProperty() {
		return this.nazivUloge;
	}
	

	public String getNazivUloge() {
		return this.nazivUlogeProperty().get();
	}
	

	public void setNazivUloge(String nazivUloge) {
		this.nazivUlogeProperty().set(nazivUloge);
	}
	

	public StringProperty imeProperty() {
		return this.ime;
	}
	

	public String getIme() {
		return this.imeProperty().get();
	}
	

	public void setIme(String ime) {
		this.imeProperty().set(ime);
	}
	

	public StringProperty prezimeProperty() {
		return this.prezime;
	}
	

	public String getPrezime() {
		return this.prezimeProperty().get();
	}
	

	public void setPrezime(String prezime) {
		this.prezimeProperty().set(prezime);
	}
	

	public ObjectProperty<LocalDate> datumRodjenjaProperty() {
		return this.datumRodjenja;
	}
	

	public LocalDate getDatumRodjenja() {
		return this.datumRodjenjaProperty().get();
	}
	

	public void setDatumRodjenja(LocalDate datumRodjenja) {
		this.datumRodjenjaProperty().set(datumRodjenja);
	}
	

	public ObjectProperty<LocalDate> datumPotpisivanjaProperty() {
		return this.datumPotpisivanja;
	}
	

	public LocalDate getDatumPotpisivanja() {
		return this.datumPotpisivanjaProperty().get();
	}
	

	public void setDatumPotpisivanja(LocalDate datumPotpisivanja) {
		this.datumPotpisivanjaProperty().set(datumPotpisivanja);
	}
	

	public ObjectProperty<LocalDate> datumOdProperty() {
		return this.datumOd;
	}
	

	public LocalDate getDatumOd() {
		return this.datumOdProperty().get();
	}
	

	public void setDatumOd(LocalDate datumOd) {
		this.datumOdProperty().set(datumOd);
	}
	

	public ObjectProperty<LocalDate> datumDoProperty() {
		return this.datumDo;
	}
	

	public LocalDate getDatumDo() {
		return this.datumDoProperty().get();
	}
	

	public void setDatumDo(LocalDate datumDo) {
		this.datumDoProperty().set(datumDo);
	}
	

	public StringProperty honorarProperty() {
		return this.honorar;
	}
	

	public String getHonorar() {
		return this.honorarProperty().get();
	}
	

	public void setHonorar(String honorar) {
		this.honorarProperty().set(honorar);
	}
	
	
	
}
