package bp.dto;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KompanijaDTO {

	private IntegerProperty idKompanije;
	private StringProperty nazivKompanije;
	private StringProperty adresa;
	
	public Integer getIdKompanije() {
		return idKompanije.get();
	}

	public void setIdKompanije(Integer idKompanije) {
		this.idKompanije.set(idKompanije);
	}

	public String getNazivKompanije() {
		return nazivKompanije.get();
	}

	public void setNazivKompanije(String nazivKompanije) {
		this.nazivKompanije.set(nazivKompanije);
	}

	public String getAdresa() {
		return adresa.get();
	}

	public void setAdresa(String adresa) {
		this.adresa.set(adresa==null?"":adresa);
	}

	public String toString() {
		return nazivKompanije.get()+" "+adresa.get();
	}
	public KompanijaDTO(Integer idKompanije,String nazivKompanije,String adresa) {
		this.idKompanije=new SimpleIntegerProperty(idKompanije);
		this.nazivKompanije=new SimpleStringProperty(nazivKompanije);
		this.adresa=new SimpleStringProperty(adresa==null?"":adresa);
	}
	
}
