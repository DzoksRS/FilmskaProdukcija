package bp.dto;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EMailDTO {

	private StringProperty mail;
	
	public String getMail() {
		return mail.get();
	}

	public void setMail(String email) {
		this.mail.set(email);
	}

	public EMailDTO(String email) {
		this.mail=new SimpleStringProperty(email);
		System.out.println(this.mail.get());
	}
}