package bp.dto;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class SezonaDTO {
	private IntegerProperty brojSezone;
	private IntegerProperty idDjela;
	private IntegerProperty brojEpizodaUSezoni;
	public SezonaDTO(Integer brojSezone,Integer idDjela,Integer brojEpizodaUSezoni) {
		this.brojSezone=new SimpleIntegerProperty(brojSezone);
		this.idDjela=new SimpleIntegerProperty(idDjela);
		this.brojEpizodaUSezoni=new SimpleIntegerProperty(brojEpizodaUSezoni);
	}
	public IntegerProperty brojSezoneProperty() {
		return this.brojSezone;
	}
	
	public int getBrojSezone() {
		return this.brojSezoneProperty().get();
	}
	
	public void setBrojSezone(final int brojSezone) {
		this.brojSezoneProperty().set(brojSezone);
	}
	
	public IntegerProperty idDjelaProperty() {
		return this.idDjela;
	}
	
	public int getIdDjela() {
		return this.idDjelaProperty().get();
	}
	
	public void setIdDjela(final int idDjela) {
		this.idDjelaProperty().set(idDjela);
	}
	
	public IntegerProperty brojEpizodaUSezoniProperty() {
		return this.brojEpizodaUSezoni;
	}
	
	public int getBrojEpizodaUSezoni() {
		return this.brojEpizodaUSezoniProperty().get();
	}
	
	public void setBrojEpizodaUSezoni(final int brojEpizodaUSezoni) {
		this.brojEpizodaUSezoniProperty().set(brojEpizodaUSezoni);
	}
	
	
}
